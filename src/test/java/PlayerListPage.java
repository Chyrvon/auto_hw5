import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class PlayerListPage extends BasePage {

    @FindBy(xpath = IDEN_INSERT_BUTTON)
    public WebElement _insert;

    @FindBy(xpath = IDEN_LOGOUT_BUTTON)
    public WebElement _logoutLink;

    @FindBy(id = IDEN_SEARCH_PLAEYR_FIELD)
    public WebElement _player_field;

    @FindBy(xpath = IDEN_SEARCH_BUTTON)
    public WebElement _search_button;

    @FindBy(id = IDEN_INS_USERNAME_FIELD)
    public WebElement _username_field;

    @FindBy(id = IDEN_INS_EMAIL_FIELD)
    public WebElement _Email;

    @FindBy(id = IDEN_INS_FIRSTNAME_FIELD)
    public WebElement _FirstName;

    @FindBy(id = IDEN_INS_LASTNAME_FIELD)
    public WebElement _LastName;

    @FindBy(id = IDEN_INS_CITY_FIELD)
    public WebElement _City;

    @FindBy(id = IDEN_INS_ADDRESS_FIELD)
    public WebElement _Address;

    @FindBy(id = IDEN_INS_PHONE_FIELD)
    public WebElement _Phone;

    @FindBy(id = IDEN_INS_COUNTRY_FIELD)
    public WebElement _County;

    @FindBy(id = IDEN_INS_SUBMIT_BUTTON)
    public WebElement _submit_button;

    @FindBy(xpath= IDEN_RESET_BUTTON)
    public WebElement _reset_button;



    public PlayerListPage(WebDriver driver) {
        super(driver);
    }

    public void insert(WebDriver driver){
        //wait Clickable Insert button and click
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(_insert));
        _insert.click();
        //check the Title of page - "Players - Insert"
        Assert.assertEquals(driver.getTitle(), TITLE_INSERT_PAGE);
    }

    public void find_and_VerifyPlayer(WebDriver driver, PokerPlayer p0) {
        Assert.assertEquals(driver.getTitle(), TITLE_PLAYERS_PAGE);

        //wait visibility Player field and enter Username
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.elementToBeClickable(_search_button));

        _player_field.sendKeys(p0.Username);
        System.out.println("Search Player: " + p0.Username);
        _search_button.click();

        String IDEN_VERIFY_USER = IDEN_VERIFY_USER_START + p0.Username + IDEN_VERIFY_USER_END;
        WebElement _edit_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_VERIFY_USER)));
        _edit_button.click();

        //wait Clickable Submit button
        wait.until(ExpectedConditions.elementToBeClickable(_submit_button));
        Assert.assertEquals(driver.getTitle(), TITLE_EDIT_PAGE);

        PokerPlayer p3 = new PokerPlayer();
        p3.Username = _username_field.getAttribute("value");
        p3.Password = p1.Password;
        p3.Confirm_Password = p1.Confirm_Password;
        p3.Email = _Email.getAttribute("value");
        p3.FirstName = _FirstName.getAttribute("value");
        p3.LastName = _LastName.getAttribute("value");
        p3.City = _City.getAttribute("value");
        p3.Address = _Address.getAttribute("value");
        p3.Phone = _Phone.getAttribute("value");
        Select select = new Select(_County);
        p3.County = select.getFirstSelectedOption().getText();

        Assert.assertEquals(p3, p0);

        System.out.println("\nVerify data Player - OK");
        _submit_button.click();
    }

    public void deletePlayer(WebDriver driver, PokerPlayer p0) {
        Assert.assertEquals(driver.getTitle(), TITLE_PLAYERS_PAGE);


        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.elementToBeClickable(_search_button));
        _player_field.sendKeys(p0.Username);
        System.out.println("Search Player: " + p0.Username);
        _search_button.click();

        //wait Clickable Delete button and click
        WebElement _delete_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_DELETE_USER)));
        _delete_button.click();

        //Alert, click OK
        Alert alert;
        alert = driver.switchTo().alert();
        alert.accept();
        Assert.assertEquals(driver.getTitle(), TITLE_PLAYERS_PAGE);

        //wait Clickable Reset button and click
        wait.until(ExpectedConditions.elementToBeClickable(_reset_button));
        _reset_button.click();

        System.out.println("Delete Player: " + p0.Username + " - OK");
    }

    public void logout(WebDriver driver){
        //wait Clickable Logout button and click
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(_logoutLink));
        _logoutLink.click();
        Assert.assertEquals(driver.getTitle(), TITLE_LOGIN_PAGE);
        System.out.println("Operation LogOut: OK");
    }
}
