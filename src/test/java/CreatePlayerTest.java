import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class CreatePlayerTest extends BaseTest{

    private LoginPage loginPage;
    private PlayerListPage playerListPage;
    private CreatePlayerPage createPlayerPage;

    @BeforeSuite(groups = {"CreatePlayer", "EditPlayer", "DeletePlayer"})
    public void initPages() {
        loginPage = new LoginPage(driver);
        loginPage.init();
        playerListPage = new PlayerListPage(driver);
        playerListPage.init();
        createPlayerPage = new CreatePlayerPage(driver);
        createPlayerPage.init();
    }

    @Test(groups = {"CreatePlayer"}, priority = 0)
    public void CreatePlayer(){
        driver.manage().window().maximize();
        driver.get(OPENED_LINK_PAGE);
        loginPage.login(LOG_USERNAME, LOG_PASSWORD, driver);
        Assert.assertEquals(TITLE_PLAYERS_PAGE, playerListPage.getTitle());
        playerListPage.insert(driver);
        createPlayerPage.createPlayer(driver, p1);
        playerListPage.find_and_VerifyPlayer(driver, p1);
        playerListPage.deletePlayer(driver, p1);
        playerListPage.logout(driver);
    }





}

