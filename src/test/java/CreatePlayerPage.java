import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CreatePlayerPage extends BasePage{

    @FindBy(id = IDEN_INS_USERNAME_FIELD)
    public WebElement _username_field;

    @FindBy(id = IDEN_INS_EMAIL_FIELD)
    public WebElement _email_field;

    @FindBy(id = IDEN_INS_PASSWORD_FIELD)
    public WebElement _password_field;

    @FindBy(id = IDEN_INS_CONFIRM_PASSWORD_FIELD)
    public WebElement _confirm_field;

    @FindBy(id = IDEN_INS_FIRSTNAME_FIELD)
    public WebElement _firstname;

    @FindBy(id = IDEN_INS_LASTNAME_FIELD)
    public WebElement _lastname;

    @FindBy(id = IDEN_INS_COUNTRY_FIELD)
    public WebElement _county;

    @FindBy(id = IDEN_INS_CITY_FIELD)
    public WebElement _city;

    @FindBy(id = IDEN_INS_ADDRESS_FIELD)
    public WebElement _address;

    @FindBy(id = IDEN_INS_PHONE_FIELD)
    public WebElement _phone;

    @FindBy(id = IDEN_INS_SUBMIT_BUTTON)
    public WebElement _submit_button;

    public CreatePlayerPage(WebDriver driver) {
        super(driver);
    }

    public void createPlayer(WebDriver driver, PokerPlayer p0){

        //wait Clickable Submit button and click
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(_submit_button));

        _username_field.sendKeys(p0.Username);
        _email_field.sendKeys(p0.Email);
        _password_field.sendKeys(p0.Password);
        _confirm_field.sendKeys(p0.Confirm_Password);
        _firstname.sendKeys(p0.FirstName);
        _lastname.sendKeys(p0.LastName);
        _county.sendKeys(p0.County);
        _city.sendKeys(p0.City);
        _address.sendKeys(p0.Address);
        _phone.sendKeys(p0.Phone);
        _submit_button.click();

        System.out.println("Operation Create Player: OK");
    }
}
