import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;

public class BaseTest extends Variable{
    public WebDriver driver;

    @BeforeSuite(groups = {"CreatePlayer", "EditPlayer", "DeletePlayer"})
    public void setupDriver(){
        System.out.println("Start TESTING");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        System.out.println("SetupWebDriver: OK");

        //SetupTestData
        p1.Username = DATA_P1_USERNAME;
        p1.Email = DATA_P1_EMAIL;
        p1.Password = DATA_P1_PASSWORD;
        p1.Confirm_Password = DATA_P1_CONFIRM_PASSWORD;
        p1.FirstName = DATA_P1_FIRSTNAME;
        p1.LastName = DATA_P1_LASTNAME;
        p1.City = DATA_P1_CITY;
        p1.County = DATA_P1_COUNTRY;
        p1.Address = DATA_P1_ADDRESS;
        p1.Phone = DATA_P1_PHONE;

        p3.Username = DATA_P3_USERNAME;
        p3.Email = DATA_P3_EMAIL;
        p3.Password = DATA_P3_PASSWORD;
        p3.Confirm_Password = DATA_P3_CONFIRM_PASSWORD;
        p3.FirstName = DATA_P3_FIRSTNAME;
        p3.LastName = DATA_P3_LASTNAME;
        p3.City = DATA_P3_CITY;
        p3.County = DATA_P3_COUNTRY;
        p3.Address = DATA_P3_ADDRESS;
        p3.Phone = DATA_P3_PHONE;

        p2.Username = DATA_P3_USERNAME;
        p2.Email = DATA_P2_EMAIL;
        p2.FirstName = DATA_P2_FIRSTNAME;
        p2.LastName = DATA_P2_LASTNAME;
        p2.City = DATA_P2_CITY;
        p2.County = DATA_P2_COUNTRY;
        p2.Address = DATA_P2_ADDRESS;
        p2.Phone = DATA_P2_PHONE;

        p4.Username = DATA_P4_USERNAME;
        p4.Email = DATA_P4_EMAIL;
        p4.Password = DATA_P4_PASSWORD;
        p4.Confirm_Password = DATA_P4_CONFIRM_PASSWORD;
        p4.FirstName = DATA_P4_FIRSTNAME;
        p4.LastName = DATA_P4_LASTNAME;
        p4.City = DATA_P4_CITY;
        p4.County = DATA_P4_COUNTRY;
        p4.Address = DATA_P4_ADDRESS;
        p4.Phone = DATA_P4_PHONE;
        System.out.println("SetupTestData: OK");
    }

    @AfterSuite(groups = {"CreatePlayer", "EditPlayer", "DeletePlayer"})
    public void tearDown(){
        sleep (1000);
        driver.quit();
        System.out.println("Close the Chrome");
    }


    public void sleep (int msecs){
        try {
            Thread.sleep(msecs);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
