import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class LoginPage extends BasePage {

    @FindBy(id=IDEN_LOG_USERNAME_FIELD)
    public WebElement _username;

    @FindBy(id=IDEN_LOG_PASSWORD_FIELD)
    public WebElement _password;

    @FindBy(id=IDEN_LOG_BUTTON)
    public WebElement _btnLogin;

    @FindBy(xpath=IDEN_ERROR_MESSAGE)
    public WebElement _errorMessage;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void login (String username, String password, WebDriver driver){
        //wait open Login page -> wait Clickable LogIn button
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(_btnLogin));

        //check the Title of page - "Login"
        Assert.assertEquals(driver.getTitle(), TITLE_LOGIN_PAGE);

        _username.sendKeys(username);
        _password.sendKeys(password);
        _btnLogin.click();
        System.out.println("Operation Login: OK");
    }

    public String getErrorMessage(){
        return _errorMessage.getText();
    }

}
