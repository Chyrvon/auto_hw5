import java.util.Objects;

public class PokerPlayer {
    public String Username;
    public String Email;
    public String Password;
    public String Confirm_Password;
    public String FirstName;
    public String LastName;
    public String City;
    public String County;
    public String Address;
    public String Phone;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PokerPlayer that = (PokerPlayer) o;
        return Objects.equals(Username, that.Username) &&
                Objects.equals(Email, that.Email) &&
                Objects.equals(Password, that.Password) &&
                Objects.equals(Confirm_Password, that.Confirm_Password) &&
                Objects.equals(FirstName, that.FirstName) &&
                Objects.equals(LastName, that.LastName) &&
                Objects.equals(City, that.City) &&
                Objects.equals(County, that.County) &&
                Objects.equals(Address, that.Address) &&
                Objects.equals(Phone, that.Phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Username, Email, Password, Confirm_Password, FirstName, LastName, City, County, Address, Phone);
    }

    @Override
    public String toString() {
        return "PokerPlayer{" +
                "Username='" + Username + '\'' +
                ", Email='" + Email + '\'' +
                ", Password='" + Password + '\'' +
                ", Confirm_Password='" + Confirm_Password + '\'' +
                ", FirstName='" + FirstName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", City='" + City + '\'' +
                ", County='" + County + '\'' +
                ", Address='" + Address + '\'' +
                ", Phone='" + Phone + '\'' +
                '}';
    }
}

