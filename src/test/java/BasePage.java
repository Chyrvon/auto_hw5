import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

class Variable {
    ///////     Variable definition     ///////
    //Title page
    static final String TITLE_LOGIN_PAGE = "Login";
    static final String TITLE_PLAYERS_PAGE = "Players";
    static final String TITLE_INSERT_PAGE = "Players - Insert";
    static final String TITLE_EDIT_PAGE = "Players - Edit";

    //Data
    static final String OPENED_LINK_PAGE = "http://80.92.229.235/auth/login";
    static final String LOG_USERNAME = "admin";
    static final String LOG_PASSWORD = "test";

    //p1
    static final String DATA_P1_USERNAME = "Zx" + ((int) Math.floor(Math.random()*(999999 - 9999))+9999);
    static final String DATA_P1_EMAIL = DATA_P1_USERNAME + "@ukr.net";
    static final String DATA_P1_PASSWORD = "Qwerty@123456";
    static final String DATA_P1_CONFIRM_PASSWORD = "Qwerty@123456";
    static final String DATA_P1_FIRSTNAME = "Auto_FirstName";
    static final String DATA_P1_LASTNAME = "Auto_LastName";
    static final String DATA_P1_CITY = "Kharkiv";
    static final String DATA_P1_COUNTRY = "UKRAINE";
    static final String DATA_P1_ADDRESS = "Nauky ave. 14";
    static final String DATA_P1_PHONE = "0001234567";

    //p3
    static final String DATA_P3_USERNAME = "Zx" + ((int) Math.floor(Math.random()*(999999 - 9999))+9999);
    static final String DATA_P3_EMAIL = DATA_P3_USERNAME + "@ukr.net";
    static final String DATA_P3_PASSWORD = "Qwerty@123456";
    static final String DATA_P3_CONFIRM_PASSWORD = "Qwerty@123456";
    static final String DATA_P3_FIRSTNAME = "Auto_FirstName_P3";
    static final String DATA_P3_LASTNAME = "Auto_LastName_P3";
    static final String DATA_P3_CITY = "Kharkiv";
    static final String DATA_P3_COUNTRY = "UKRAINE";
    static final String DATA_P3_ADDRESS = "Nauky ave. XX";
    static final String DATA_P3_PHONE = "9991234567";

    //p3 upgrade player
    static final String DATA_P2_EMAIL = DATA_P3_USERNAME + "@gmail.net";
    static final String DATA_P2_FIRSTNAME = "Upgrade_FirstName";
    static final String DATA_P2_LASTNAME = "Upgrade_LastName";
    static final String DATA_P2_CITY = "Kyiv";
    static final String DATA_P2_COUNTRY = "CANADA";
    static final String DATA_P2_ADDRESS = "Lenina ave. 14";
    static final String DATA_P2_PHONE = "1117654321";

    //p4
    static final String DATA_P4_USERNAME = "Zxc" + ((int) Math.floor(Math.random()*(999999 - 999))+999);
    static final String DATA_P4_EMAIL = DATA_P4_USERNAME + "@ukr.net";
    static final String DATA_P4_PASSWORD = "Qwerty@123456";
    static final String DATA_P4_CONFIRM_PASSWORD = "Qwerty@123456";
    static final String DATA_P4_FIRSTNAME = "Auto_FirstName_P4";
    static final String DATA_P4_LASTNAME = "Auto_LastName_P4";
    static final String DATA_P4_CITY = "Kharkiv";
    static final String DATA_P4_COUNTRY = "UKRAINE";
    static final String DATA_P4_ADDRESS = "Nauky ave. XXX";
    static final String DATA_P4_PHONE = "2221234560";


    //Field identifiers
    static final String IDEN_LOG_USERNAME_FIELD = "username";
    static final String IDEN_LOG_PASSWORD_FIELD = "password";
    static final String IDEN_LOG_BUTTON = "logIn";
    static final String IDEN_INSERT_BUTTON = "//a[contains(text(), \"Insert\")]";
    static final String IDEN_INS_USERNAME_FIELD = "us_login";
    static final String IDEN_INS_EMAIL_FIELD = "us_email";
    static final String IDEN_INS_PASSWORD_FIELD = "us_password";
    static final String IDEN_INS_CONFIRM_PASSWORD_FIELD = "confirm_password";
    static final String IDEN_INS_FIRSTNAME_FIELD = "us_fname";
    static final String IDEN_INS_LASTNAME_FIELD = "us_lname";
    static final String IDEN_INS_COUNTRY_FIELD = "us_country";
    static final String IDEN_INS_CITY_FIELD = "us_city";
    static final String IDEN_INS_ADDRESS_FIELD = "us_address";
    static final String IDEN_INS_PHONE_FIELD = "us_phone";
    static final String IDEN_INS_SUBMIT_BUTTON = "Submit";
    static final String IDEN_SEARCH_PLAEYR_FIELD = "login";
    static final String IDEN_SEARCH_BUTTON = "//button[contains(text(), \"Search\")]";
    static final String IDEN_RESET_BUTTON = "//button[contains(text(), \"Reset\")]";
    static final String IDEN_EDIT_USER = "//a[text() = \"" + DATA_P3_USERNAME + "\"]/../../td[1]/a";
    static final String IDEN_DELETE_USER = "//a[text() = \"" + DATA_P1_USERNAME + "\"]/../../td[16]/a";
    static final String IDEN_DELETE_USER_P4 = "//a[text() = \"" + DATA_P4_USERNAME + "\"]/../../td[16]/a";
    static final String IDEN_CONFIRM_DELETE_USER ="//td[@class = \"dataTables_empty\"]";
    static final String IDEN_VERIFY_USER_START = "//a[text() = \"";
    static final String IDEN_VERIFY_USER_END = "\"]/../../td[1]/a";
    static final String IDEN_LOGOUT_BUTTON = "//div[@class=\"logout\"]/a";
    static final String IDEN_ERROR_MESSAGE = "//div[@class=\"logout\"]/a";


    public static PokerPlayer p1 = new PokerPlayer();
    public static PokerPlayer p2 = new PokerPlayer();
    public static PokerPlayer p3 = new PokerPlayer();
    public static PokerPlayer p4 = new PokerPlayer();
}



public class BasePage extends Variable{

    private WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public BasePage init () {
        PageFactory.initElements(driver, this);
        return this;
    }

    public String getTitle() {
        return driver.getTitle();
    }
}
